# ◦ Load

### Latest
```html
<script src="https://philippn.com/vendor/console/dist.js"></script>
```

### A certain version
```html
<script src="https://philippn.com/vendor/console/v0.0.1/dist.js"></script>
```

### Composer (PHP Packagist)

```bash
composer require projs/console
```

```html
<script src="/vendor/projs/console/dist.js"></script>
```

### NPM @todo

# ◦ Use

### Config
```js
const Console = new _Console();
Console
    //.setWriteToConsole(true) // Control the whole output in browser console
    //.setWarn(false) // Use a simple 'console.log()' (no backtrace included), otherwise 'console.warn()'
    //.setDebug(true) // Use JS 'debugger'
    .setLevels(['click', 'watch']) // 'placeholder' not included by default
;
```

### Default usage
```js
Console.log('Text here', 'Text here2'); // Output is optional here
```

### To see only placeholders
```js
Console.setLevels(['placeholder']); // Other levels are not logged

Console.placeholder(); // Other levels are not logged
```

### To see placeholders and a certain level
```js
Console.setLevels(['watch', 'placeholder']);

Console.level('watch', 'Text here'); // Output is optional here
Console.placeholder('Text here');
```

### Force log
```js
Console._log('Text here'); // Output is optional here
```

### Force log and debug
```js
Console.__log('Text here'); // Output is optional here
```

