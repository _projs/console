const _Console = function () {
    this.writeToConsole = false;
    this.debug = false;
    this.warn = true;

    this.levels = [];
}

_Console.prototype.setWriteToConsole = function (bool) {
    this.writeToConsole = bool;
    return this;
}
_Console.prototype.setDebug = function (bool) {
    this.debug = bool;
    return this;
}
_Console.prototype.setWarn = function (bool) {
    this.warn = bool;
    return this;
}
_Console.prototype.setLevels = function (levels) {
    this.levels = levels;
    return this;
}


_Console.prototype.log = function () {

    if (this.writeToConsole) {
        var args = Array.prototype.slice.call(arguments);

        if (
            args.length === 0 ||
            (!args[0].startsWith("%cLevel:") && !args[0].startsWith("%cPLACEHOLDER"))
        ) {
            args = ["%cDEFAULT", 'color: blue; font-style: italic'].concat(args);
        }

        if (this.warn) {
            console.warn.apply(console, args);
        } else {
            console.log.apply(console, args);
        }
    }

    if (this.debug) {
        debugger;
    }
}


_Console.prototype.level = function () {
    var args = Array.prototype.slice.call(arguments);

    if (
        this.levels.length === 0 ||
        JSON.stringify(this.levels) == JSON.stringify(['placeholder']) ||
        this.levels.includes(args[0])
    ) {
        // @todo Create a separate config later
        args = ["%cLevel: " + args[0] + "\n", 'color: blue; font-weight: bold'].concat(args.slice(1));
        this.log.apply(this, args);
    }
}
_Console.prototype.placeholder = function () {
    if (this.levels.includes('placeholder')) {
        var args = Array.prototype.slice.call(arguments);
        // @todo Create a separate config later
        args = ["%cPLACEHOLDER", 'color: blue; font-style: italic'].concat(args);
        this.log.apply(this, args);
    }
}

_Console.prototype._log = function () {
    var args = Array.prototype.slice.call(arguments);
    console.warn.apply(console, args);
}
_Console.prototype.__log = function () {
    var args = Array.prototype.slice.call(arguments);
    console.warn.apply(console, args);

    debugger;
}